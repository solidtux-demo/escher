#!/usr/bin/env python3

import demo
import json
import pprint

data = json.loads(demo.webgl_default())
pp = pprint.PrettyPrinter(indent=4)

data['shader']['keyframes'] = [0, 5, 10]
data['shader']['length'] = 10
data['shader']['constants']['max_depth'] = 100
data['shader']['constants']['max_iter'] = 100
data['shader']['constants']['max_step'] = 1
data['shader']['constants']['eps'] = 0.001
data['shader']['camera']['position'] = { 'smooth': [
        {'static': (-15, 4, 10)},
        {'static': (-10, 15, 10)},
        {'static': (-15, 4, 10)},
    ] }
data['shader']['camera']['zoom']['static'] = 1.
box = { 
        'objecttype': {
            'box': { 'size': { 'static': (2., 1., 4.) } }
            },
        'id': 0
        }
plane = { 
        'objecttype': 'plane',
        'id': 0
        }
sphere = { 
        'objecttype': {
            'sphere': { 'radius': { 'static': 0.5 } }
            },
        'id': 1
        }
box_text = {
        'texturetype': { 'color': { 'static': (1., 1., 0.8) } },
        'ambient': { 'static': 0.3 },
        'diffuse': { 'static': 0.7 },
        'specular': { 'static': 0 },
        'shininess': { 'static': 4 },
        }
sphere_text = {
        'texturetype': { 'color': { 'static': (1., 0, 0) } },
        'ambient': { 'static': 0.3 },
        'diffuse': { 'static': 0.7 },
        'specular': { 'static': 0 },
        'shininess': { 'static': 4 },
        }
data['shader']['scene']['operations'] = [
        { 'expression': 'if (p.y < 6.) {' },
        { 'expression': 'p.xy += mod(TIME, 1.)*vec2(2., 1.);' },
        { 'expression': 'p.xy = vec2(p.x - 2.*floor(p.y), mod(p.y, 1.));' },
        { 'set': box },
        { 'move': { 'static': (2, 1, 0) } },
        { 'union': box },
        { 'move': { 'static': (-4, -2, 0) } },
        { 'union': box },
        { 'expression': '}' },
        'reset',
        { 'move': { 'static': (0, 3, 0) } },
        { 'union': plane },
        'reset',
        { 'move': { 'static': (1.75, -1.5, 0) } },
        { 'move': { 'expression': 'vec3(0., -8.*(mod(TIME, 1.) - mod(TIME, 1.)*mod(TIME, 1.)), 0.)' } },
        { 'union': sphere },
        ]
data['shader']['scene']['textures'].append(box_text)
data['shader']['scene']['textures'].append(sphere_text)
data['shader']['lights'].append({ 'position': { 'expression': 'vec3(15.*cos(0.2*PI*TIME), 4, 15.*sin(0.2*PI*TIME))' }, 'color': { 'static': (1, 1, 1) } })
data['shader']['postprocessing'].append({ 'dark': {
    'density': { 'static': 1 },
    'min_depth': { 'static': 30 },
    'max_depth': { 'static': 50 },
    } })

s = json.dumps(data, sort_keys=True, indent=4)
with open('data.json', 'w') as f:
    f.write(s)
demo.webgl_json(s)
demo.shadertoy_json(s)
